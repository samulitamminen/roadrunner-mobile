import React, {Component} from 'react';
import {View, AppRegistry} from 'react-native';
import {Provider} from 'react-redux';

import Store from './src/redux/Store';
import RootComponent from './src/root/RootComponent';
import BleComponent from './src/ble/BleComponent';
import LocationComponent from './src/location/LocationComponent';

class RoadRunner extends Component {
  render() {
    return (
      <Provider store={Store}>
        <View style={{flex: 1}}>
          <BleComponent/>
          <LocationComponent/>
          <RootComponent/>
        </View>
      </Provider>
    );
  }
}

AppRegistry.registerComponent('RoadRunner', () => RoadRunner);
