# Roadrunner Mobile

## Prerequisites

- iOS or Android mobile device with Bluetooth 4 support
- Roadrunner electric skateboard or a mocking device

## Setup

- Install [React Native](https://facebook.github.io/react-native/docs/getting-started.html#content)
- Install [yarn](https://yarnpkg.com/lang/en/)
- Install dependencies `yarn install`
- Run the app `react-native run-android`

## Android release build

Create file `scripts/config` containing your keystore password

    ROADRUNNER_KEYSTORE_PASSWORD=<keystore-password>

Add the following to your `~/.gradle/gradle.properties` file.

    org.gradle.daemon=true
    ​
    ROADRUNNER_RELEASE_STORE_FILE=roadrunner-release.keystore
    ROADRUNNER_RELEASE_KEY_ALIAS=roadrunner-release
    ROADRUNNER_RELEASE_STORE_PASSWORD=<keystore-password>
    ROADRUNNER_RELEASE_KEY_PASSWORD=<keystore-password>

Then run the script

    cd scripts
    ./build-release-android.sh
