// https://www.materialui.co/flatuicolors

export const Color = {
  carrot: '#e67e22',
  wetAsphalt: '#34495e',
  clouds: '#ecf0f1',
  silver: '#bdc3c7',
  white: 'white',
  nephritis: '#27ae60',
};

export const Default = {
  padding: 16,
};
