import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Geolib from 'geolib';

import * as actions from '../redux/board/Actions';
import * as selectors from '../redux/board/Reducer';

/*global navigator */ // Define global navigator object for ESLint

class LocationComponent extends Component {
  componentWillReceiveProps(newProps) {
    const options = {
      enableHighAccuracy: true,
      distanceFilter: 3,
    };

    if (newProps.shouldWatchPosition && !this.watchId) {
      navigator.geolocation.getCurrentPosition(
        (loc) => this.locationReceived(loc),
        (error) => console.warn('LocationComponent getCurrentPosition:', JSON.stringify(error, null, 2)),
        options);
      this.watchId = navigator.geolocation.watchPosition(
        (loc) => this.locationReceived(loc),
        (error) => console.warn('LocationComponent watchPosition:', JSON.stringify(error, null, 2)),
        options
      );
    }
    if (!newProps.shouldWatchPosition) {
      navigator.geolocation.clearWatch(this.watchId);
    }
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchId);
  }

  watchId = null;
  desiredAccuracy = 15;
  lastSavedLocation = null;

  locationReceived(location) {
    if (this.isAccurate(location)) {
      if (this.differsEnoughFromLast(location)) {
        this.props.newLocation(location);
      } else {
        // console.log('Location is too close to last saved: ', location.coords);
      }
    } else {
      // console.log('Location accuracy too poor: ', location.coords.accuracy,'>', this.desiredAccuracy);
      // TODO: Show some warning indicator?
    }
  }

  isAccurate(location) {
    return location.coords.accuracy < this.desiredAccuracy;
  }

  differsEnoughFromLast(location) {
    if (this.lastSavedLocation) {
      const distance = Geolib.getDistance(
        {
          latitude: location.coords.latitude,
          longitude: location.coords.longitude,
        },
        {
          latitude: this.lastSavedLocation.coords.latitude,
          longitude: this.lastSavedLocation.coords.longitude,
        }
      );
      if (distance > (2 * location.coords.accuracy)) {
        this.lastSavedLocation = location;
        return true;
      }
      return false;
    }
    this.lastSavedLocation = location;
    return true;
  }

  render() {
    return null;
  }
}

LocationComponent.propTypes = {
  shouldWatchPosition: React.PropTypes.bool.isRequired,
  newLocation: React.PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  shouldWatchPosition: selectors.isInLiveMode(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  newLocation: actions.newLocation,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(LocationComponent);
