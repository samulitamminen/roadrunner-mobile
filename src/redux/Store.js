import {AsyncStorage} from 'react-native';
import {applyMiddleware, createStore} from 'redux';
import createLogger from 'redux-logger';
import thunk from 'redux-thunk';
import {persistStore, autoRehydrate} from 'redux-persist';

import {EXECUTE_TRANSACTION, COMPLETE_TRANSACTION} from './ble/ActionTypes';
import reducer from './Reducer';

const middlewares = [thunk];

if (process.env.NODE_ENV === 'development') {
  const logger = createLogger({
    predicate: (getState, action) => {
      return action.type !== EXECUTE_TRANSACTION &&
             action.type !== COMPLETE_TRANSACTION;
    },
    collapsed: true,
  });
  middlewares.push(logger);
}

// Create the store
const store = createStore(
  reducer,
  applyMiddleware(...middlewares),
  autoRehydrate()
);

persistStore(store, {
  storage: AsyncStorage,
  whitelist: ['board'],
});

export default store;
