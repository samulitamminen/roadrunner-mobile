// @flow
import type {
  ParameterType,
  RideItem,
  Ride,
} from '../../Types';
import {isValidParameter} from '../../services/BoardProtocol';
import * as types from './ActionTypes';
import {round} from '../../helpers/Utils';

const initialState = {
  isInLiveMode: false,
  rides: [],
  currentRide: [],
};

type State = Object;

export default function reducer(state: State = initialState, action: Object) {
  switch (action.type) {
    case types.RECEIVED_FRAME:
      return receivedFrame(state, action);
    case types.LIVE_MODE:
      return {...state, isInLiveMode: action.value};
    case types.END_RIDE:
      return endRide(state);
    case types.START_RIDE:
      return startRide(state);
    case types.DELETE_RIDE:
      return deleteRide(state, action);
    case types.NEW_LOCATION:
      return newLocation(state, action);
    default:
      return state;
  }
}

function roundTimestamp(timestamp: number): number {
  return round(timestamp, 100);
}

function newLocation(state: State, action: Object) {
  const timestamp = roundTimestamp(action.location.timestamp);
  const currentRide = [...state.currentRide];
  if (!currentRide.find((data) => data.timestamp === timestamp)) {
    currentRide.push({
      timestamp,
    });
  }
  const index = currentRide.findIndex((data) => data.timestamp === timestamp);
  currentRide[index] = {
    ...currentRide[index],
    latitude: action.location.coords.latitude,
    longitude: action.location.coords.longitude,
    altitude: action.location.coords.altitude,
  };
  return {
    ...state,
    currentRide,
  };
}

function receivedFrame(state: State, action: Object) {
  const frame = action.frame;
  if (!isValidParameter(frame.parameterId)) {
    console.warn('invalid parameter received:', frame.parameterId);
    return state;
  }
  const timestamp = roundTimestamp(frame.timestamp);
  const currentRide = [...state.currentRide];
  if (!currentRide.find((data) => data.timestamp === timestamp)) {
    currentRide.push({
      timestamp,
    });
  }
  const index = currentRide.findIndex((data) => data.timestamp === timestamp);
  currentRide[index][frame.parameterId] = frame.value;
  return {
    ...state,
    currentRide,
  };
}

function startRide(state: State): State {
  return {
    ...state,
    currentRide: [],
  };
}

function endRide(state: State): State {
  if (state.currentRide.length === 0) {
    return state;
  }

  const ride = state.currentRide.sort((a, b) => a.timestamp > b.timestamp ? 1 : -1);
  return {
    ...state,
    rides: [ride, ...state.rides],
    currentRide: [],
  };
}

function deleteRide(state: State, action: Object): State {
  return {
    ...state,
    rides: state.rides.filter(ride => ride[0].timestamp !== action.startingTime),
  };
}

// Selectors
export function getLatestParameterData(state: Object, parameter: ParameterType): ?number {
  for (let i = state.board.currentRide.length - 1; i >= 0; i--) {
    if (state.board.currentRide[i][parameter]) {
      return state.board.currentRide[i][parameter];
    }
  }
  return null;
}

export function getParameterData(state: Object, parameter: ParameterType): [RideItem] {
  return state.board.currentRide
  .filter((data) => data[parameter])
  .map((data) => {
    return {
      timestamp: data.timestamp,
      value: data[parameter],
    };
  });
}

export function isInLiveMode(state: Object): boolean {
  return state.board.isInLiveMode;
}

export function getRides(state: Object): [?Ride] {
  return state.board.rides;
}

export function getCurrentRide(state: Object): Ride {
  return state.board.currentRide;
}
