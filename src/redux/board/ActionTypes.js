
export const RECEIVED_FRAME = 'RECEIVED_FRAME';
export const LIVE_MODE = 'LIVE_MODE';
export const START_RIDE = 'START_RIDE';
export const END_RIDE = 'END_RIDE';
export const DELETE_RIDE = 'DELETE_RIDE';
export const NEW_LOCATION = 'NEW_LOCATION';
