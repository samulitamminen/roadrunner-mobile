// @flow
import type {
  Action,
  ThunkAction,
  Frame,
  ParameterType,
  Location,
} from '../../Types';
import * as types from './ActionTypes';
import {sendToDevice} from '../ble/Actions';
import {
  composeReadFrame,
  composeWriteFrame,
  base64ToFrame,
  Parameter,
} from '../../services/BoardProtocol';

// Receiving Data frames from the board
// TODO: Refactor these names
export const receivedFrame = (frame: Frame): Action => ({
  type: types.RECEIVED_FRAME,
  frame,
});

export const frameReceived = (encodedString: string): ThunkAction => {
  return (dispatch) => {
    const frame = base64ToFrame(encodedString);
    frame.timestamp = Date.now();
    return dispatch(receivedFrame(frame));
  };
};

export const readParameter = (parameter: ParameterType) =>
  sendToDevice(composeReadFrame(parameter));

export const writeParameter = (parameter: ParameterType, value: number) =>
  sendToDevice(composeWriteFrame(parameter, value));

export const liveMode = (value: boolean): Action => ({
  type: types.LIVE_MODE,
  value,
});

export const setLiveMode = (value: boolean): ThunkAction => {
  return (dispatch) => {
    const numericValue = value ? 1 : 0;
    dispatch(liveMode(value));
    dispatch(writeParameter(Parameter.LiveMode, numericValue));
  };
};

export const startRide = (): ThunkAction => {
  return (dispatch) => {
    dispatch(setLiveMode(true));
    dispatch({type: types.START_RIDE});
  };
};

export const endRide = (): ThunkAction => {
  return (dispatch) => {
    dispatch(setLiveMode(false));
    dispatch({type: types.END_RIDE});
  };
};

export const deleteRide = (startingTime: number): Action => ({
  type: types.DELETE_RIDE,
  startingTime,
});

export const newLocation = (location: Location): Action => ({
  type: types.NEW_LOCATION,
  location,
});
