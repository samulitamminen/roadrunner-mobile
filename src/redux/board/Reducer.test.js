import * as types from './ActionTypes';
import reducer from './Reducer';
import * as selectors from './Reducer';
import * as actions from './Actions';

const initialState = {
  isInLiveMode: false,
  rides: [],
  currentRide: [],
};

describe('Board Reducer', () => {
  it('returns the initial state', () => {
    const initial = reducer(undefined, {}); // eslint-disable-line no-undefined
    expect(initial).toMatchSnapshot();
  });

  it('turns on live mode', () => {
    const next = reducer(initialState, actions.liveMode(true));
    expect(next).toMatchSnapshot();
  });

  it('turns off live mode', () => {
    const next = reducer(
      {...initialState, isInLiveMode: true},
      actions.liveMode(false)
    );
    expect(next).toMatchSnapshot();
  });

  it('adds new data to empty current ride', () => {
    const next = reducer(
      initialState,
      actions.receivedFrame({
        parameterId: 'T',
        value: 987.6,
        timestamp: 1486463475123,
      })
    );
    expect(next).toMatchSnapshot();
  });

  it('adds new data to current ride with existing data', () => {
    const state = stateWithExistingData();
    const next = reducer(
      state,
      actions.receivedFrame({
        parameterId: 'V',
        value: 43.6,
        timestamp: 1486463475140,
      })
    );
    expect(next).toMatchSnapshot();
  });

  it('saves current ride when ending it', () => {
    const state = stateWithExistingData();
    const next = reducer(state, {type: types.END_RIDE});
    expect(next).toMatchSnapshot();
  });
});

describe('Board Selector', () => {
  const state = {board: stateWithExistingData()};

  it('get latest parameter data when where is multiple', () => {
    const latest = selectors.getLatestParameterData(state, 'T');
    expect(latest).toMatchSnapshot();
  });
  it('get latest parameter data when where is one', () => {
    const latest = selectors.getLatestParameterData(state, 'V');
    expect(latest).toMatchSnapshot();
  });
  it('get latest parameter data when where is none', () => {
    const latest = selectors.getLatestParameterData(state, 'd');
    expect(latest).toMatchSnapshot();
  });

  it('get all parameter data when there is data', () => {
    const data = selectors.getParameterData(state, 'T');
    expect(data).toMatchSnapshot();
  });
  it('get all parameter data when there is missing data', () => {
    const data = selectors.getParameterData(state, 'V');
    expect(data).toMatchSnapshot();
  });
  it('get all parameter data when there is not data', () => {
    const data = selectors.getParameterData(state, 'd');
    expect(data).toMatchSnapshot();
  });

  it('is in live mode', () => {
    expect(selectors.isInLiveMode(state)).toMatchSnapshot();
  });

  it('get all rides', () => {
    const withSavedRide = reducer(state.board, {type: types.END_RIDE});
    expect(selectors.getRides({board: withSavedRide})).toMatchSnapshot();
  });
});

function stateWithExistingData() {
  let state = reducer(initialState, actions.receivedFrame({
    parameterId: 'T',
    value: 123.4,
    timestamp: 1486463475123,
  }));
  state = reducer(state, actions.receivedFrame({
    parameterId: 'V',
    value: 43,
    timestamp: 1486463475125,
  }));
  state = reducer(state, actions.receivedFrame({
    parameterId: 'T',
    value: 999,
    timestamp: 1486463475600,
  }));
  return state;
}
