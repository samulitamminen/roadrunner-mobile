import {combineReducers} from 'redux';

import ble from './ble/Reducer';
import board from './board/Reducer';

const reducer = combineReducers({
  ble,
  board,
});

export default reducer;
