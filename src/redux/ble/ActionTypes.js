// Actions
export const START_SCAN = 'START_SCAN';
export const STOP_SCAN = 'STOP_SCAN';
export const DEVICE_FOUND = 'DEVICE_FOUND';
export const SET_STATUS = 'SET_STATUS';
export const SET_IS_CONNECTED = 'SET_IS_CONNECTED';
export const SET_SELECTED_DEVICE = 'SET_SELECTED_DEVICE';
export const SET_SELECTED_SERVICE = 'SET_SELECTED_SERVICE';
export const SET_SELECTED_CHARACTERISTIC = 'SET_SELECTED_CHARACTERISTIC';
export const DISCONNECT_FROM_DEVICE = 'DISCONNECT_FROM_DEVICE';

// RX/TX
export const SEND_TO_DEVICE = 'SEND_TO_DEVICE';
export const READ_FROM_DEVICE = 'READ_FROM_DEVICE';
export const MONITOR_DEVICE = 'MONITOR_DEVICE';
export const EXECUTE_TRANSACTION = 'EXECUTE_TRANSACTION';
export const COMPLETE_TRANSACTION = 'COMPLETE_TRANSACTION';
