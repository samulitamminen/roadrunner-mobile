import * as types from './ActionTypes';
import {END_OF_FRAME, base64ToString} from '../../services/BoardProtocol';
import {frameReceived} from '../board/Actions';
import {delay} from '../../helpers/Utils';

// Device state enum
export const DeviceStatus = {
  Disconnect: 'Disconnect',
  Disconnecting: 'Disconnecting',
  Disconnected: 'Disconnected',
  Connect: 'Connect',
  Connecting: 'Connecting',
  Connected: 'Connected',
  Scanning: 'Scanning',
  Fetching: 'Fetching services and charasteristics',
  Discovering: 'Discovering',
  Monitoring: 'Monitoring',
};

export const autoConnect = () => {
  return async (dispatch, getState) => {
    await delay(1000);
    await dispatch(startScan());
    await delay(1500);
    if (getState().ble.devices.length > 0) {
      await dispatch(setSelectedService('0000ffe0-0000-1000-8000-00805f9b34fb'));
      await dispatch(setSelectedCharacteristic('0000ffe1-0000-1000-8000-00805f9b34fb'));
      await dispatch(setSelectedDevice('782A9F03-39C2-17DB-2553-FE9C83C10267'));
      await delay(2000);
      await dispatch(monitorDevice());
    }
  };
};

// Action Creators
export const startScan = () => ({
  type: types.START_SCAN,
});

export const stopScan = () => ({
  type: types.STOP_SCAN,
});

export const deviceFound = device => ({
  type: types.DEVICE_FOUND,
  device,
});

export const setStatus = status => ({
  type: types.SET_STATUS,
  status,
});

export const setIsConnected = isConnected => ({
  type: types.SET_IS_CONNECTED,
  isConnected,
});

export const setSelectedDevice = id => ({
  type: types.SET_SELECTED_DEVICE,
  id,
});

export const disconnectFromDevice = () => ({
  type: types.DISCONNECT_FROM_DEVICE,
});

export const setSelectedService = id => ({
  type: types.SET_SELECTED_SERVICE,
  id,
});

export const setSelectedCharacteristic = id => ({
  type: types.SET_SELECTED_CHARACTERISTIC,
  id,
});

export const sendToDevice = message => ({
  type: types.SEND_TO_DEVICE,
  message,
});

export const readFromDevice = () => ({
  type: types.READ_FROM_DEVICE,
});

export const monitorDevice = () => ({
  type: types.MONITOR_DEVICE,
});

export const receivedFromDevice = encodedMessage => {
  return (dispatch) => {
    const receivedString = base64ToString(encodedMessage);
    if (receivedString.length === 5 && receivedString.slice(-1) === END_OF_FRAME) {
      // The whole frame sent at once
      dispatch(frameReceived(encodedMessage));
    } else {
      console.warn('Invalid frame received: ', receivedString);
    }
  };
};

export const executeTransaction = transactionId => ({
  type: types.EXECUTE_TRANSACTION,
  transactionId,
});

export const completeTransaction = transactionId => ({
  type: types.COMPLETE_TRANSACTION,
  transactionId,
});
