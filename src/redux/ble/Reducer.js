import * as actions from './ActionTypes';
import {DeviceStatus} from './Actions';

// Default state
const defaultState = {
  selectedDevice: null,
  scannedDevices: [],
  selectedDeviceId: null,
  selectedServiceId: null,
  selectedCharacteristicId: null,
  status: DeviceStatus.Disconnected,
  isConnected: false,
  operations: {},
  transactionId: 0,
};

// Reducer
// eslint-disable-next-line complexity
export default function reducer(state = defaultState, action = {}) {
  const transactionId = state.transactionId;

  switch (action.type) {
    case actions.START_SCAN:
      return {...state, status: DeviceStatus.Scanning, scannedDevices: []};
    case actions.STOP_SCAN:
      return {...state, status: DeviceStatus.Disconnected};
    case actions.DEVICE_FOUND:
      return {...state, scannedDevices: [...state.scannedDevices, action.device]};
    case actions.SET_STATUS:
      return {...state, status: action.status};
    case actions.SET_IS_CONNECTED:
      return {...state, isConnected: action.isConnected};
    case actions.SET_SELECTED_DEVICE:
      return setSelectedDevice(state, action);
    case actions.DISCONNECT_FROM_DEVICE:
      return disconnectFromCurrentDevice(state);
    case actions.SET_SELECTED_SERVICE:
      return {...state, selectedServiceId: action.id};
    case actions.SET_SELECTED_CHARACTERISTIC:
      return {...state, selectedCharacteristicId: action.id};
    case actions.SEND_TO_DEVICE:
      return sendToDevice(state, action, transactionId);
    case actions.READ_FROM_DEVICE:
      return readFromDevice(state, transactionId);
    case actions.MONITOR_DEVICE:
      return monitorDevice(state, transactionId);
    case actions.EXECUTE_TRANSACTION:
      return executeTransaction(state, action);
    case actions.COMPLETE_TRANSACTION:
      return completeTransaction(state, action);
    default:
      return state;
  }
}

function setSelectedDevice(state, action) {
  return {
    ...state,
    status: DeviceStatus.Connect,
    selectedDeviceId: action.id,
    selectedDevice: state.scannedDevices.find((device) => device.id === action.id),
  };
}

function disconnectFromCurrentDevice(state) {
  return {
    ...state,
    status: DeviceStatus.Disconnect,
  };
}

function sendToDevice(state, action, transactionId) {
  const operations = {...state.operations};
  operations[transactionId] = {
    type: 'write',
    state: 'new',
    message: action.message,
    transactionId,
  };
  return {...state, operations, transactionId: state.transactionId + 1};
}

function readFromDevice(state, transactionId) {
  const operations = {...state.operations};
  operations[transactionId] = {
    type: 'read',
    state: 'new',
    transactionId,
  };
  return {...state, operations, transactionId: state.transactionId + 1};
}

function monitorDevice(state, transactionId) {
  const operations = {...state.operations};
  operations[transactionId] = {
    type: 'monitor',
    state: 'new',
    transactionId,
  };
  return {
    ...state,
    operations,
    transactionId: state.transactionId + 1,
    status: DeviceStatus.Monitoring,
  };
}

function executeTransaction(state, action) {
  const operations = {...state.operations};
  operations[action.transactionId].state = 'inProgress';
  return {...state, operations};
}

function completeTransaction(state, action) {
  const operations = {...state.operations};
  delete operations[action.transactionId];
  return {...state, operations};
}

// Selectors
export function getDeviceList(state) {
  return state.ble.scannedDevices;
}

export function isScanning(state) {
  return state.ble.status === DeviceStatus.Scanning;
}

export function isConnected(state) {
  return state.ble.isConnected;
}

export function getStatus(state) {
  return state.ble.status;
}

export function getSelectedDevice(state) {
  return state.ble.selectedDevice;
}

export function getSelectedDeviceId(state) {
  return state.ble.selectedDeviceId;
}

export function getSelectedServiceId(state) {
  return state.ble.selectedServiceId;
}

export function getSelectedCharacteristicId(state) {
  return state.ble.selectedCharacteristicId;
}
