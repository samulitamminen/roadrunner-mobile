import React from 'react';
import {
  StyleSheet,
  ScrollView,
} from 'react-native';

import Container from '../components/Container';
import NavigationBar from '../components/NavigationBar';
import {Default} from '../styles/Styles';
import SectionHeader from '../components/SectionHeader';

const DashboardView = () => {
  return (
    <Container>
      <NavigationBar title='Dashboard' />
      <ScrollView contentContainerStyle={styles.contentContainer}>
        <SectionHeader title='Placeholder for battery, stats and ride mode' />
      </ScrollView>
    </Container>
  );
};

const styles = StyleSheet.create({
  contentContainer: {
    paddingBottom: Default.padding,
  },
});

export default DashboardView;
