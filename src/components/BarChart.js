import React from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';

const BARS = 20;

let chartWidth = 0;
let chartHeight = 0;
let barWidth = 0;

let maxValue = 0;

const BarChart = ({barColor, data}) => {
  const chartData = data.slice(data.length - BARS, data.length - 1);
  maxValue = chartData.reduce((max, item) => {
    return item.value > max ? item.value : max;
  }, 0);

  return (
    <View style={[styles.chartArea]} onLayout={handleOnLayout}>
      {
        chartData.map((item, i) => (
          <View
            key={i}
            style={[
              styles.bar,
              {
                width: barWidth,
                height: item.value / maxValue * chartHeight,
                borderTopLeftRadius: barWidth / 2,
                borderTopRightRadius: barWidth / 2,
                backgroundColor: barColor,
                borderBottomColor: barColor,
              },
            ]} />
        ))
      }
    </View>
  );
};

BarChart.propTypes = {
  barColor: React.PropTypes.string.isRequired,
  data: React.PropTypes.array.isRequired,
};

function handleOnLayout(event) {
  let {width, height} = event.nativeEvent.layout;
  chartWidth = width;
  chartHeight = height;
  barWidth = Math.ceil(chartWidth / BARS);
}

const styles = StyleSheet.create({
  chartArea: {
    flex: 1,
    flexDirection: 'row',
    alignSelf: 'stretch',
    justifyContent: 'flex-end',
  },
  bar: {
    borderBottomWidth: 1,
    flexDirection: 'row',
    alignSelf: 'flex-end',
  },
});

export default BarChart;
