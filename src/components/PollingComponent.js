import React from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Timer from 'react-native-timer';

import {isConnected} from '../redux/ble/Reducer';
import {readParameter} from '../redux/board/Actions';

class PollingComponent extends React.Component {
  componentDidMount() {
    // If pollingRate is not defined, do not poll
    if (this.props.isConnected && this.props.pollingRate) {
      Timer.setInterval(this, 'dataPolling', () => {
        this.props.readParameter(this.props.boardParameter);
      }, this.props.pollingRate);
    }
  }

  componentWillReceiveProps(newProps) {
    if (newProps.isConnected && !this.props.isConnected && this.props.pollingRate) {
      Timer.setInterval(this, 'dataPolling', () => {
        this.props.readParameter(this.props.boardParameter);
      }, this.props.pollingRate);
    } else if (!newProps.isConnected && this.props.isConnected) {
      Timer.clearInterval(this);
    }
  }

  componentWillUnmount() {
    Timer.clearInterval(this);
  }

  render() {
    return this.props.children;
  }
}

PollingComponent.propTypes = {
  boardParameter: React.PropTypes.string.isRequired,
  pollingRate: React.PropTypes.number,
  parameterData: React.PropTypes.array,
  isConnected: React.PropTypes.bool,
  children: React.PropTypes.oneOfType([
    React.PropTypes.arrayOf(React.PropTypes.node),
    React.PropTypes.node,
  ]),

  readParameter: React.PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  isConnected: isConnected(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  readParameter,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(PollingComponent);
