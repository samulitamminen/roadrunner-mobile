import React from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';
import Button from 'apsl-react-native-button';

import {Color, Default} from '../styles/Styles';

const MyButton = ({style, textStyle, onPress, children}) => (
  <Button
    style={[styles.button, style]}
    textStyle={[styles.buttonText, textStyle]}
    onPress={onPress}
  >
    {children}
  </Button>
);

MyButton.propTypes = {
  style: View.propTypes.style,
  textStyle: Text.propTypes.style,
  onPress: React.PropTypes.func,
  children: React.PropTypes.node,
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: Color.carrot,
    borderWidth: 0,
    margin: Default.padding,
  },
  buttonText: {
    color: Color.white,
  },
});

export default MyButton;
