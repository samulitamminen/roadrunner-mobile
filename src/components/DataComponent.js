import React from 'react';
import {connect} from 'react-redux';
import {
  StyleSheet,
  View,
  Text,
} from 'react-native';

import PollingComponent from './PollingComponent';
import {getLatestParameterData} from '../redux/board/Reducer';
import {Color, Default} from '../styles/Styles';

class DataComponent extends React.Component {
  render() {
    return (
      <PollingComponent
        boardParameter = {this.props.boardParameter}
        pollingRate={this.props.pollingRate}
      >
        <View style={styles.container}>
          <Text style={styles.title}>{this.props.title}</Text>
          <Text style={styles.value}>{this.props.latestValue}</Text>
          <Text style={styles.unit}>{this.props.unit}</Text>
        </View>
      </PollingComponent>
    );
  }
}

DataComponent.propTypes = {
  title: React.PropTypes.string.isRequired,
  unit: React.PropTypes.string.isRequired,
  boardParameter: React.PropTypes.string.isRequired,
  pollingRate: React.PropTypes.number,
  latestValue: React.PropTypes.number,
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignSelf: 'stretch',
    alignItems: 'center',
    padding: Default.padding,
    backgroundColor: Color.white,
    borderBottomWidth: 1,
    borderBottomColor: Color.silver,
  },
  title: {
    flex: 2,
    fontWeight: 'bold',
  },
  value: {
    flex: 1,
    fontSize: 24,
  },
  unit: {
    flex: 1,
    fontSize: 12,
  },
});

const mapStateToProps = (state, ownProps) => ({
  latestValue: getLatestParameterData(state, ownProps.boardParameter),
});

export default connect(mapStateToProps, null)(DataComponent);
