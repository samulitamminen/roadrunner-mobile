import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
} from 'react-native';

import {Color, Default} from '../styles/Styles';

const ListItem = ({primaryText, secondaryText, onPress}) => {
  return (
    <TouchableHighlight
      onPress={onPress}
      style={styles.listItem}
      underlayColor={Color.silver}
    >
      <View>
        <Text style={styles.primary}>{primaryText}</Text>
        <Text style={styles.secondary}>{secondaryText}</Text>
      </View>
    </TouchableHighlight>
  );
};

ListItem.propTypes = {
  primaryText: React.PropTypes.string,
  secondaryText: React.PropTypes.string,
  onPress: React.PropTypes.func,
};

const styles = StyleSheet.create({
  listItem: {
    //flex: 1,
    padding: Default.padding,
    borderBottomWidth: 1,
    borderBottomColor: Color.silver,
    backgroundColor: Color.white,
  },
  primary: {
    fontSize: 20,
  },
  secondary: {
    fontSize: 14,
    paddingTop: 8,
    color: Color.silver,
  },
});

export default ListItem;
