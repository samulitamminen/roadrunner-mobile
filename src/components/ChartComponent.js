import React from 'react';
import {connect} from 'react-redux';
import {
  StyleSheet,
  View,
  Text,
} from 'react-native';

import PollingComponent from './PollingComponent';
import BarChart from './BarChart';
import {
  getLatestParameterData,
  getParameterData,
} from '../redux/board/Reducer';
import {Color, Default} from '../styles/Styles';

class ChartComponent extends React.Component {
  render() {
    return (
      <PollingComponent
        boardParameter = {this.props.boardParameter}
        pollingRate={this.props.pollingRate}
      >
        <View style={styles.container}>
          <View style={styles.half}>
            <Text style={styles.title}>{this.props.title} ({this.props.unit})</Text>
            <BarChart barColor='#e64a19' data={this.props.parameterData} />
          </View>
          <View style={[styles.half, styles.valueContainer]}>
            <Text style={styles.value}>{this.props.latestParameterData}</Text>
            <Text style={styles.unit}>{this.props.unit}</Text>
          </View>
        </View>
      </PollingComponent>
    );
  }
}

ChartComponent.propTypes = {
  title: React.PropTypes.string.isRequired,
  unit: React.PropTypes.string.isRequired,
  boardParameter: React.PropTypes.string.isRequired,
  pollingRate: React.PropTypes.number,

  parameterData: React.PropTypes.array,
  latestParameterData: React.PropTypes.number,
};

const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
    alignItems: 'flex-start',
    flexDirection: 'row',
    padding: Default.padding,
    backgroundColor: Color.white,
    borderBottomWidth: 1,
    borderBottomColor: Color.silver,
    height: 150,
  },
  half: {
    flex: 1,
  },
  title: {
    fontWeight: 'bold',
    marginBottom: 10,
  },
  valueContainer: {
    flex: 1,
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  value: {
    fontSize: 40,
  },
  unit: {
    fontSize: 30,
    paddingLeft: 10,
  },
});

const mapStateToProps = (state, ownProps) => ({
  parameterData: getParameterData(state, ownProps.boardParameter),
  latestParameterData: getLatestParameterData(state, ownProps.boardParameter),
});

export default connect(mapStateToProps, null)(ChartComponent);
