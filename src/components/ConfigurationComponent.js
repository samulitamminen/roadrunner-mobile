import React from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
} from 'react-native';

import {writeParameter} from '../redux/board/Actions';
import {Color, Default} from '../styles/Styles';

class ConfigurationComponent extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>{this.props.title}</Text>
        <TextInput
          style={styles.input}
          placeholder='New value'
          returnKeyType='send'
          onSubmitEditing={event => {
            this.props.writeParameter(this.props.boardParameter, event.nativeEvent.text);
          }}
        />
      </View>
    );
  }
}

ConfigurationComponent.propTypes = {
  title: React.PropTypes.string.isRequired,
  boardParameter: React.PropTypes.string.isRequired,

  writeParameter: React.PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignSelf: 'stretch',
    alignItems: 'center',
    padding: Default.padding,
    backgroundColor: Color.white,
    borderBottomWidth: 1,
    borderBottomColor: Color.silver,
  },
  title: {
    flex: 1,
    fontWeight: 'bold',
  },
  input: {
    flex: 1,
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
  },
});

const mapDispatchToProps = dispatch => bindActionCreators({
  writeParameter,
}, dispatch);

export default connect(null, mapDispatchToProps)(ConfigurationComponent);
