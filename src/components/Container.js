import React from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';

import {Color} from '../styles/Styles';

const Container = (props) => {
  return (
    <View style={styles.container}>
      {props.children}
    </View>
  );
};

Container.propTypes = {
  children: React.PropTypes.node,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: Color.clouds,
    paddingBottom: 50, // TabBar
  },
});

export default Container;
