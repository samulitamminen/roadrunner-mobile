import React from 'react';
import {
  StyleSheet,
  Platform,
  Text,
  View,
} from 'react-native';

import {Color, Default} from '../styles/Styles';

const NavigationBar = (props) => {
  return (
    <View style={styles.container}>
      <View style={styles.left}>{props.left}</View>
      <Text style={styles.title}>{props.title}</Text>
      <View style={styles.right}>{props.right}</View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: Default.padding,
    paddingRight: Default.padding,
    backgroundColor: Color.white,
    borderBottomWidth: 1,
    borderBottomColor: Color.silver,
    alignItems: 'center',
    flexDirection: 'row',
    ...Platform.select({
      ios: {
        height: 64,
        paddingTop: 20, // iOS Status Bar
      },
      android: {
        height: 62,
      },
    }),
  },
  left: {
    ...Platform.select({
      ios: {
        flex: 1,
      },
      android: {
        paddingRight: Default.padding,
      },
    }),
  },
  title: {
    ...Platform.select({
      ios: {
        textAlign: 'center',
        fontSize: 17,
      },
      android: {
        textAlign: 'left',
        fontSize: 20,
      },
    }),
    color: Color.wetAsphalt,
    flex: 2,
  },
  right: {
    flex: 1,
    alignItems: 'flex-end',
  },
});

NavigationBar.propTypes = {
  left: React.PropTypes.node,
  title: React.PropTypes.string,
  right: React.PropTypes.node,
};

export default NavigationBar;
