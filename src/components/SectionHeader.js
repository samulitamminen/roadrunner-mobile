import React from 'react';
import {
  StyleSheet,
  Text,
  View,
} from 'react-native';

import {Color} from '../styles/Styles';

const SectionHeader = ({title}) => (
  <View style={styles.section}>
    <Text style={styles.sectionTitle}>{title}</Text>
  </View>
);

SectionHeader.propTypes = {
  title: React.PropTypes.string.isRequired,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: Color.clouds,
  },
  section: {
    padding: 16,
    borderBottomWidth: 1,
    borderBottomColor: Color.silver,
  },
  sectionTitle: {
    fontSize: 16,
  },
});

export default SectionHeader;
