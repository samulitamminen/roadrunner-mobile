import React from 'react';
import {
  TouchableOpacity,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/MaterialIcons';

import {Color} from '../styles/Styles';

export default function backButton() {
  return (
    <TouchableOpacity onPress={() => Actions.pop()}>
      <Icon name='arrow-back' size={24} color={Color.wetAsphalt} />
    </TouchableOpacity>
  );
}
