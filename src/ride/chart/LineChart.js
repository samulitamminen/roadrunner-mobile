import React from 'react';
import {
  StyleSheet,
  ART,
  View,
  Text,
  InteractionManager,
  ActivityIndicator,
} from 'react-native';

const {
  Group,
  Shape,
  Surface,
} = ART;

import {Color} from '../../styles/Styles';
import {createLineGraph} from './graphUtils';
import {msToHHMMSS} from '../../helpers/Utils';

class LineChart extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      renderPlaceholder: true,
    };
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      this.computeNextState(this.props);
    });
  }

  computeNextState(nextProps) {
    const lineGraph = createLineGraph({
      data: nextProps.data,
      width: nextProps.width,
      height: nextProps.height,
    });

    this.setState({
      linePath: lineGraph.path,
      ticks: lineGraph.ticks,
      minY: lineGraph.minY,
      maxY: lineGraph.maxY,
    });
  }

  formatXTick(time) {
    return time === 0 ? '0' : msToHHMMSS(time);
  }

  render() {
    if (!this.state.linePath) {
      return <ActivityIndicator />;
    }
    return (
      <View style={styles.container}>
        <View style={styles.chartRow}>
          <View style={styles.chart}>
            <Surface
              width={this.props.width}
              height={this.props.height}
            >
              <Group x={0} y={0}>
                <Shape
                  d={this.state.linePath}
                  stroke={Color.carrot}
                  strokeWidth={2}
                />
              </Group>
            </Surface>
          </View>
          <View style={styles.axisY}>
            <Text style={styles.tickY}>{this.state.maxY.toFixed(1)}</Text>
            <Text style={styles.tickY}>{this.state.minY.toFixed(1)}</Text>
          </View>
        </View>
        <View style={styles.axisX}>
          {
            this.state.ticks.map((tick, index) => {
              const tickWidth = this.props.width / 6;
              const tickStyles = {
                position: 'absolute',
                textAlign: 'center',
                fontSize: 10,
                lineHeight: 16,
                width: tickWidth,
                left: tick.x - tickWidth / 2,
              };
              return (
                <Text key={index} style={tickStyles}>
                  {this.formatXTick(tick.time)}
                </Text>
              );
            })
          }
        </View>
      </View>
    );
  }
}

LineChart.propTypes = {
  data: React.PropTypes.array.isRequired,
  width: React.PropTypes.number.isRequired,
  height: React.PropTypes.number.isRequired,
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Color.white,
  },
  chartRow: {
    flexDirection: 'row',
  },
  chart: {
    borderTopWidth: 1,
    borderBottomWidth: 1,
    margin: 5,
    borderColor: Color.silver,
  },
  axisX: {
    height: 16,
  },
  axisY: {
    justifyContent: 'space-between',
  },
  tickY: {
    fontSize: 10,
  },
});

export default LineChart;
