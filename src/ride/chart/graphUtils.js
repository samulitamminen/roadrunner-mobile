import * as scale from 'd3-scale';
import * as shape from 'd3-shape';

const d3 = {
  scale,
  shape,
};

export function createLineGraph({
  data,
  width,
  height,
}) {
  const minX = data[0].timestamp;
  const maxX = data[data.length - 1].timestamp;
  const scaleX = d3.scale.scaleTime()
    .domain([new Date(minX), new Date(maxX)])
    .range([0, width]);

  const maxY = data.reduce((max, item) => {
    return item.value > max ? item.value : max;
  }, data[0].value);

  const minY = data.reduce((min, item) => {
    return item.value < min ? item.value : min;
  }, data[0].value);

  const scaleY = d3.scale.scaleLinear()
    .domain([minY, maxY]).nice()
    .range([height, 0]); // Inverted for React

  const lineShape = d3.shape.line()
    .x(d => scaleX(d.timestamp))
    .y(d => scaleY(d.value));

  return {
    data,
    scale: {
      x: scaleX,
      y: scaleY,
    },
    path: lineShape(data),
    minY,
    maxY,
    ticks: [0, 1, 2, 3, 4]
      .map(n => {
        const tickTime = n * (maxX - minX) / 4;
        return {
          x: scaleX(new Date(minX + tickTime)),
          time: tickTime,
        };
      }),
  };
}
