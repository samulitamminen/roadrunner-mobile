import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
} from 'react-native';

import {Color, Default} from '../styles/Styles';
import LineChart from './chart/LineChart';
import {
  parametersOfRide,
  squashChartData,
} from '../services/rideHelpers';
import {parameterName} from '../services/BoardProtocol';

const {width} = Dimensions.get('window');

const RideAnalysis = ({ride}) => {
  return (
    <View style={styles.container}>
      {
        parametersOfRide(ride).map(parameter => {
          return (
          <View key={parameter} style={styles.chart}>
            <Text>{parameterName(parameter)}</Text>
            <LineChart
              data={dataForChart(parameter, ride)}
              width={width - Default.padding * 5}
              height={100}
            />
          </View>
          );
        })
      }
    </View>
  );
};

function dataForChart(parameter, ride) {
  const chartData = ride
    .filter(item => item[parameter])
    .map(item => ({
      timestamp: item.timestamp,
      value: item[parameter],
    }));

  const points = 100;
  return squashChartData(chartData, points);
}

RideAnalysis.propTypes = {
  ride: React.PropTypes.array,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    backgroundColor: Color.white,
  },
  chart: {
    padding: Default.padding,
  },
});

export default RideAnalysis;
