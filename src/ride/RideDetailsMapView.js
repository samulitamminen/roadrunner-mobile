import React from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';

import Container from '../components/Container';
import NavigationBar from '../components/NavigationBar';
import BackButton from '../components/NavigationBackButton';
import RideMap from './RideMap';
import {startingDate, coordinates} from '../services/rideHelpers';

const RideDetailsMapView = ({ride}) => {
  return (
    <Container>
      <NavigationBar
        left={<BackButton/>}
        title={startingDate(ride)}
      />
      <View style={styles.map}>
        <RideMap coordinates={coordinates(ride)} initialFit />
      </View>
    </Container>
  );
};

RideDetailsMapView.propTypes = {
  ride: React.PropTypes.array.isRequired,
};

const styles = StyleSheet.create({
  map: {
    flex: 1,
  },
});

export default RideDetailsMapView;
