import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
  StyleSheet,
} from 'react-native';
import {Actions} from 'react-native-router-flux';

import Button from '../components/Button';
import * as actions from '../redux/board/Actions';
import * as selectors from '../redux/board/Reducer';
import {Color} from '../styles/Styles';

const StartStopRideButton = (props) => {
  return (
      <Button
        onPress={() => {
          if (props.isInLiveMode) {
            props.endRide();
            Actions.pop();
          } else {
            props.startRide();
          }
        }}
        style={[styles.button, props.isInLiveMode ? styles.endButton : null]}
        textStyle={styles.buttonText}
      >
        {props.isInLiveMode ? 'End Ride' : 'Start Ride'}
      </Button>
  );
};

StartStopRideButton.propTypes = {
  isInLiveMode: React.PropTypes.bool,
  startRide: React.PropTypes.func,
  endRide: React.PropTypes.func,
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: Color.nephritis,
    borderWidth: 0,
    marginBottom: 0,
  },
  endButton: {
    backgroundColor: Color.carrot,
  },
  buttonText: {
    color: Color.white,
  },
});

const mapStateToProps = state => ({
  isInLiveMode: selectors.isInLiveMode(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  startRide: actions.startRide,
  endRide: actions.endRide,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(StartStopRideButton);
