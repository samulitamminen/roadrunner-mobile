import React from 'react';
import {
  StyleSheet,
} from 'react-native';
import MapView from 'react-native-maps';

class RideMap extends React.Component {
  componentDidMount() {
    if (this.props.initialFit) {
      setTimeout(() => {
        this.zoomToFit();
      }, 300);
    }
  }

  zoomToCoordinate(latlng) {
    if (!this.map) {
      return;
    }
    this.map.animateToRegion(
      {
        latitude: latlng.latitude,
        longitude: latlng.longitude,
        latitudeDelta: 0.005,
        longitudeDelta: 0.005,
      },
      500,
    );
  }

  zoomToFit(coordinates = this.props.coordinates) {
    if (this.map && coordinates.length > 0) {
      this.map.fitToCoordinates(
        coordinates,
        {
          edgePadding: {top: 50, right: 50, bottom: 50, left: 50},
          animated: true,
        }
      );
    }
  }

  render() {
    return (
      <MapView
        ref={ref => {this.map = ref;}}
        style={styles.map}
        showsUserLocation={this.props.showsUserLocation}
      >
        <MapView.Polyline
          coordinates={this.props.coordinates}
          strokeWidth={3}
        />
      </MapView>
    );
  }
}

RideMap.propTypes = {
  coordinates: React.PropTypes.array.isRequired,
  showsUserLocation: React.PropTypes.bool,
  initialFit: React.PropTypes.bool,
};

const styles = StyleSheet.create({
  map: {
    flex: 1,
  },
});

export default RideMap;
