import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
} from 'react-native';

import {Color, Default} from '../styles/Styles';

const {width} = Dimensions.get('window');

const RideStats = ({stats}) => {
  return (
    <View style={styles.container}>
      {
        stats.map(block => (
          <View style={styles.block} key={block.name}>
            <Text style={styles.value}>{block.value}</Text>
            <Text style={styles.name}>{block.name}</Text>
          </View>
        ))
      }
    </View>
  );
};

RideStats.propTypes = {
  stats: React.PropTypes.array,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    backgroundColor: Color.white,
    paddingLeft: Default.padding,
    paddingRight: Default.padding,
  },
  block: {
    width: (width - 2 * Default.padding) / 3,
    alignItems: 'center',
    paddingTop: Default.padding,
    paddingBottom: Default.padding,
  },
  value: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  name: {
    fontSize: 12,
  },
});

export default RideStats;
