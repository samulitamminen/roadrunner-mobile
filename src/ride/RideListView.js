import React from 'react';
import {connect} from 'react-redux';
import {
  StyleSheet,
  View,
  ScrollView,
} from 'react-native';
import {Actions} from 'react-native-router-flux';

import * as selectors from '../redux/board/Reducer';
import Button from '../components/Button';
import Container from '../components/Container';
import NavigationBar from '../components/NavigationBar';
import {Color} from '../styles/Styles';
import SectionHeader from '../components/SectionHeader';
import ListItem from '../components/ListItem';

const RideView = (props) => {
  return (
    <Container>
      <NavigationBar
        title='Ride'
      />

      <SectionHeader title='Last rides' />
      <ScrollView alwaysBounceVertical={false}>
        {
          props.rides.map((ride) => (
            <ListItem
              key={ride[0].timestamp}
              primaryText={new Date(ride[0].timestamp).toLocaleString()}
              secondaryText={'Duration: ' + ride.length}
              onPress={() => Actions.rideDetailsView({ride})}
            />
          ))
        }
      </ScrollView>

      <View style={styles.buttonContainer}>
        <Button
          style={styles.button}
          onPress={() => Actions.rideLiveView()}
        >
          {props.isInLiveMode ? 'Go to Current Ride' : 'Get rollin\'!'}
        </Button>
      </View>
    </Container>
  );
};

RideView.propTypes = {
  isInLiveMode: React.PropTypes.bool,
  rides: React.PropTypes.array,
};

const styles = StyleSheet.create({
  buttonContainer: {
    height: 75, // HACK: paddings and margins seems to not work here...
  },
  button: {
    backgroundColor: Color.nephritis,
  },
});

const mapStateToProps = state => ({
  isInLiveMode: selectors.isInLiveMode(state),
  rides: selectors.getRides(state),
});

export default connect(mapStateToProps, null)(RideView);
