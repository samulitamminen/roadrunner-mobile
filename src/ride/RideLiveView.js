import React from 'react';
import {connect} from 'react-redux';
import {
  View,
  StyleSheet,
  ScrollView,
} from 'react-native';

import {Color, Default} from '../styles/Styles';
import * as selectors from '../redux/board/Reducer';
import Container from '../components/Container';
import NavigationBar from '../components/NavigationBar';
import BackButton from '../components/NavigationBackButton';
import RideMap from './RideMap';
import RideStats from './RideStats';
import StartStopRideButton from './StartStopRideButton';
import {parameterName} from '../services/BoardProtocol';
import {
  distance,
  duration,
  speed,
  coordinates,
  parametersOfRide,
} from '../services/rideHelpers';

class RideLiveView extends React.Component {
  componentDidMount() {
    /* global navigator */
    navigator.geolocation.getCurrentPosition(
      (pos) => {
        setTimeout(() => {
          this.rideMap.zoomToCoordinate({
            latitude: pos.coords.latitude,
            longitude: pos.coords.longitude,
          });
        }, 10);
      },
      (error) => console.warn('RideLiveView getCurrentPosition: ', error.message),
      {
        enableHighAccuracy: false,
      }
    );
  }

  stats(ride) {
    const locationStats = [
      {name: 'Duration', value: duration(ride)},
      {name: 'Speed (km/h)', value: speed(ride, 3)},
      {name: 'Distance (m)', value: distance(ride)},
    ];

    const parameterStats = parametersOfRide(ride)
      .map(parameter => ({
        name: parameterName(parameter),
        value: this.props.latestParameterData(parameter),
      }));

    return [...locationStats, ...parameterStats];
  }

  render() {
    return (
      <Container>
        <ScrollView alwaysBounceVertical={false}>
          <NavigationBar
            left={<BackButton/>}
            title='Current Ride'
          />
          <View style={styles.map}>
            <RideMap
              ref={ref => {this.rideMap = ref;}}
              coordinates={coordinates(this.props.ride)}
              showsUserLocation
            />
          </View>
          <View style={styles.stats}>
            <RideStats stats={this.stats(this.props.ride)} />
          </View>
          <View style={styles.button}>
            <StartStopRideButton />
          </View>
        </ScrollView>
      </Container>
    );
  }
}

RideLiveView.propTypes = {
  ride: React.PropTypes.array.isRequired,
  latestParameterData: React.PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
  map: {
    height: 300,
    borderBottomWidth: 1,
    borderBottomColor: Color.silver,
  },
  stats: {
    borderBottomWidth: 1,
    borderBottomColor: Color.silver,
  },
  button: {
    marginBottom: Default.padding,
  },
});

const mapStateToProps = state => ({
  ride: selectors.getCurrentRide(state),
  latestParameterData: parameter => selectors.getLatestParameterData(state, parameter),
});

export default connect(mapStateToProps, null)(RideLiveView);
