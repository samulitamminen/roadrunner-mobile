import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
  StyleSheet,
  View,
  ScrollView,
  TouchableOpacity,
  Share,
  Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {Actions} from 'react-native-router-flux';

import * as actions from '../redux/board/Actions';
import {rideToCsv} from '../services/RideParser';
import {parameterName} from '../services/BoardProtocol';
import Container from '../components/Container';
import NavigationBar from '../components/NavigationBar';
import BackButton from '../components/NavigationBackButton';
import SectionHeader from '../components/SectionHeader';
import Button from '../components/Button';
import {Color} from '../styles/Styles';
import RideStats from './RideStats';
import RideAnalysis from './RideAnalysis';
import RideMap from './RideMap';
import {
  startingDate,
  distance,
  duration,
  avgSpeed,
  coordinates,
  avgParameter,
  parametersOfRide,
} from '../services/rideHelpers';

class RideDetailsView extends React.Component {
  deleteButton(ride) {
    return (
      <TouchableOpacity onPress={() => this.confirmDelete(ride)}>
        <Icon name='delete' size={24} color={Color.wetAsphalt} />
      </TouchableOpacity>
    );
  }

  confirmDelete(ride) {
    Alert.alert(
      'Delete ride?',
      'Your ride will be lost.',
      [
        {text: 'Cancel', style: 'cancel', onPress: () => {}},
        {text: 'Delete', style: 'destructive', onPress: () => this.deleteRide(ride)},
      ]
    );
  }

  deleteRide(ride) {
    this.props.deleteRide(ride);
    Actions.pop();
  }

  share(ride) {
    const csvString = rideToCsv(ride);
    Share.share({message: csvString}, {dialogTitle: 'Share your ride as CSV'});
  }

  stats(ride) {
    const locationStats = [
      {name: 'Duration', value: duration(ride)},
      {name: 'Avg Speed (km/h)', value: avgSpeed(ride)},
      {name: 'Distance (m)', value: distance(ride)},
    ];

    const parameterStats = parametersOfRide(ride)
      .map(parameter => ({
        name: 'Avg ' + parameterName(parameter),
        value: avgParameter(parameter, ride).toFixed(1),
      }));

    return [...locationStats, ...parameterStats];
  }

  render() {
    return (
      <Container>
        <NavigationBar
          left={<BackButton/>}
          title={startingDate(this.props.ride)}
          right={this.deleteButton(this.props.ride)}
        />

        <ScrollView alwaysBounceVertical={false}>
          <TouchableOpacity
            style={styles.map}
            onPress={() => Actions.rideDetailsMapView({ride: this.props.ride})}
          >
            <RideMap initialFit coordinates={coordinates(this.props.ride)} />
          </TouchableOpacity>

          <SectionHeader title='Statistics' />
          <View style={styles.stats}>
            <RideStats stats={this.stats(this.props.ride)} />
          </View>

          <SectionHeader title='Analysis' />
          <RideAnalysis ride={this.props.ride}/>

          <Button onPress={() => this.share(this.props.ride)}>
            Export as CSV
          </Button>
        </ScrollView>
      </Container>
    );
  }
}

RideDetailsView.propTypes = {
  ride: React.PropTypes.array.isRequired,
  deleteRide: React.PropTypes.func.isRequired,
};

const styles = StyleSheet.create({
  map: {
    height: 200,
    borderBottomWidth: 1,
    borderBottomColor: Color.silver,
  },
  stats: {
    borderBottomWidth: 1,
    borderBottomColor: Color.silver,
  },
});

const mapDispatchToProps = dispatch => bindActionCreators({
  deleteRide: (ride) => actions.deleteRide(ride[0].timestamp),
}, dispatch);

export default connect(null, mapDispatchToProps)(RideDetailsView);
