import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {BleManager} from 'react-native-ble-plx';
import {Actions} from 'react-native-router-flux';

import * as bleActions from '../redux/ble/Actions';
import {DeviceStatus} from '../redux/ble/Actions';
import * as bleSelectors from '../redux/ble/Reducer';

class BleComponent extends Component {
  componentWillMount() {
    this.subscriptions = {};
    this.manager = new BleManager();
    this.manager.onStateChange(newState => {
      console.log('BleManager state changed:' + newState);
    });
    // this.props.autoConnect();
  }

  // eslint-disable-next-line
  async componentWillReceiveProps(newProps) {
    // Handle scanning
    if (newProps.scanning !== this.props.scanning) {
      if (newProps.scanning) {
        this.startScanning();
      } else {
        this.manager.stopDeviceScan();
      }
    }

    // Handle connection
    if (newProps.status === DeviceStatus.Connect) {
      try {
        let device = await this.manager.connectToDevice(newProps.selectedDeviceId);

        this.subscriptions[device.uuid] = device.onDisconnected(() => {
          this.props.setStatus(DeviceStatus.Disconnected);
          this.subscriptions[device.uuid].remove();
        });

        // Discover Services
        this.props.setStatus(DeviceStatus.Discovering);
        device = await device.discoverAllServicesAndCharacteristics();

        // Fetch Services and their Characteristics
        this.props.setStatus(DeviceStatus.Fetching);
        const services = await this.fetchServicesAndCharacteristicsForDevice(device);
        console.log(services);

        // TODO: Select Service and Characteristic
        this.props.setSelectedService('0000ffe0-0000-1000-8000-00805f9b34fb');
        this.props.setSelectedCharacteristic('0000ffe1-0000-1000-8000-00805f9b34fb');
        this.props.setStatus(DeviceStatus.Connected);

        // Start monitoring the device
        this.props.monitorDevice();

      } catch (error) {
        Actions.error({message: 'Connecting error: ', error});
      }
    }

    if (newProps.status === DeviceStatus.Disconnect) {
      try {
        await this.manager.cancelDeviceConnection(this.props.selectedDeviceId);
      } catch (error) {
        Actions.error({message: 'Disconnecting error: ', error});
      }
    }

    const isConnected = await this.isDeviceConnected(this.props.selectedDeviceId);
    this.props.setIsConnected(isConnected);
    if (!isConnected) {
      return; // Do not handle operations if not connected
    }

    // Handle operations
    for (const transactionId in newProps.operations) {
      if (newProps.operations.hasOwnProperty(transactionId)) {
        let operation = newProps.operations[transactionId];
        switch (operation.type) {
          case 'write':
            if (operation.state === 'new') {
              try {
                newProps.executeTransaction(transactionId);
                // eslint-disable-next-line babel/no-await-in-loop
                await this.manager.writeCharacteristicWithResponseForDevice(
                  newProps.selectedDeviceId,
                  newProps.selectedServiceId,
                  newProps.selectedCharacteristicId,
                  operation.message, // Already base64 encoded
                );
                newProps.completeTransaction(transactionId);
              } catch (error) {
                Actions.error({message: 'Write characteristic error: ', error: error.message});
              }
            }
            break;

          case 'read':
            if (operation.state === 'new') {
              try {
                newProps.executeTransaction(transactionId);
                // eslint-disable-next-line babel/no-await-in-loop
                const characteristic = await this.manager.readCharacteristicForDevice(
                  newProps.selectedDeviceId,
                  newProps.selectedServiceId,
                  newProps.selectedCharacteristicId,
                );
                newProps.completeTransaction(transactionId);
                newProps.receivedFromDevice(characteristic.value);
              } catch (error) {
                Actions.error({message: 'Read characteristic error: ', error: error.message});
              }
            }
            break;

          case 'monitor':
            if (operation.state === 'new') {
              newProps.executeTransaction(transactionId);
              // eslint-disable-next-line babel/no-await-in-loop
              this.manager.monitorCharacteristicForDevice(
                newProps.selectedDeviceId,
                newProps.selectedServiceId,
                newProps.selectedCharacteristicId,
                (error, characteristic) => {
                  if (error) {
                    //Actions.error({message: 'Monitor characteristic error: ', error});
                    newProps.completeTransaction(transactionId);
                    return;
                  }
                  newProps.receivedFromDevice(characteristic.value);
                }
              );
            }
            break;

          default:
            Actions.error({message: 'Operation type was unknown: ' + operation.type});
            break;
        }
      }
    }
  }

  componentWillUnmount() {
    this.manager.destroy();
    delete this.manager;
  }

  startScanning() {
    const uuidsToScan = ['0000ffe0-0000-1000-8000-00805f9b34fb'];
    const options = {
      allowDuplicates: false,
    };
    const listener = (error, device) => {
      if (error) {
        Actions.error({message: 'Start Scanning', error});
        this.manager.stopDeviceScan();
        this.props.setStatus(DeviceStatus.Disconnected);
        return;
      }
      this.props.deviceFound(device);
    };
    this.props.setStatus(DeviceStatus.Scanning);
    this.manager.startDeviceScan(uuidsToScan, options, listener);
  }

  async fetchServicesAndCharacteristicsForDevice(device) {
    var servicesMap = {};
    var services = await device.services();

    for (let service of services) {
      var characteristicsMap = {};
      var characteristics = await service.characteristics(); // eslint-disable-line babel/no-await-in-loop

      for (let characteristic of characteristics) {
        characteristicsMap[characteristic.uuid] = {
          uuid: characteristic.uuid,
          isReadable: characteristic.isReadable,
          isWritable: characteristic.isWritableWithResponse,
          isNotifiable: characteristic.isNotifiable,
          isNotifying: characteristic.isNotifying,
          value: characteristic.value,
        };
      }

      servicesMap[service.uuid] = {
        uuid: service.uuid,
        isPrimary: service.isPrimary,
        characteristicsCount: characteristics.length,
        characteristics: characteristicsMap,
      };
    }
    return servicesMap;
  }

  // Check after any status changes
  async isDeviceConnected(deviceId) {
    if (deviceId) {
      return await this.manager.isDeviceConnected(deviceId);
    }
    return false;
  }

  render() {
    return null;
  }
}

BleComponent.propTypes = {
  scanning: React.PropTypes.bool,
  status: React.PropTypes.string,
  selectedDeviceId: React.PropTypes.string,
  selectedServiceId: React.PropTypes.string,
  selectedCharacteristicId: React.PropTypes.string,
  operations: React.PropTypes.object,
  transactionId: React.PropTypes.number,

  deviceFound: React.PropTypes.func,
  setStatus: React.PropTypes.func,
  setIsConnected: React.PropTypes.func,
  setSelectedService: React.PropTypes.func,
  setSelectedCharacteristic: React.PropTypes.func,
  receivedFromDevice: React.PropTypes.func,
  executeTransaction: React.PropTypes.func,
  completeTransaction: React.PropTypes.func,
  autoConnect: React.PropTypes.func,
  monitorDevice: React.PropTypes.func,
};

const mapStateToProps = state => ({
  scanning: bleSelectors.isScanning(state),
  status: bleSelectors.getStatus(state),
  selectedDeviceId: bleSelectors.getSelectedDeviceId(state),
  selectedServiceId: bleSelectors.getSelectedServiceId(state),
  selectedCharacteristicId: bleSelectors.getSelectedCharacteristicId(state),
  operations: state.ble.operations,
  transactionId: state.ble.transactionId,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  setStatus: bleActions.setStatus,
  setIsConnected: bleActions.setIsConnected,
  deviceFound: bleActions.deviceFound,
  setSelectedService: bleActions.setSelectedService,
  setSelectedCharacteristic: bleActions.setSelectedCharacteristic,
  receivedFromDevice: bleActions.receivedFromDevice,
  executeTransaction: bleActions.executeTransaction,
  completeTransaction: bleActions.completeTransaction,
  autoConnect: bleActions.autoConnect,
  monitorDevice: bleActions.monitorDevice,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(BleComponent);
