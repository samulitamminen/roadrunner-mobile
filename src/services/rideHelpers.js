import Geolib from 'geolib';

import {msToHHMMSS} from '../helpers/Utils';

export function distance(ride) {
  return Geolib.getPathLength(coordinates(ride));
}

export function duration(ride) {
  if (ride.length < 2) {
    return msToHHMMSS(0);
  }
  const ms = ride[ride.length - 1].timestamp - ride[0].timestamp;
  return msToHHMMSS(ms);
}

export function avgSpeed(ride) {
  const ms = ride[ride.length - 1].timestamp - ride[0].timestamp;
  const dist = distance(ride);
  const mpers = dist / (ms / 1000) * 3.6;
  return Math.round(mpers * 100) / 100;
}

export function speed(ride, count) {
  const coords = ride.filter(item => item.latitude && item.longitude);
  const last = coords.slice(coords.length - 1 - count, coords.length - 1);
  if (last.length < count) {
    return 0;
  }

  let ms = last[last.length - 1].timestamp - last[0].timestamp;
  let dist = Geolib.getPathLength(last);

  const mpers = dist / (ms / 1000) * 3.6;
  return Math.round(mpers * 100) / 100;
}

export function startingDate(ride) {
  const date = new Date(ride[0].timestamp);
  return date.getDate() + '.' + date.getMonth() + '.' + date.getFullYear() +
    ' ' + date.getHours() + ':' + date.getMinutes();
}

export function coordinates(ride) {
  return ride
    .filter(item => item.latitude && item.longitude)
    .map(item => ({
      latitude: item.latitude,
      longitude: item.longitude,
    }));
}

export function avgParameter(parameter, ride) {
  const filteredRide = ride.filter(item => item[parameter]);
  if (filteredRide.length === 0) {
    return null;
  }
  return filteredRide.reduce((sum, item) => sum + item[parameter], 0) / filteredRide.length;
}

export function parametersOfRide(ride) {
  const ignoredKeys = ['timestamp', 'latitude', 'longitude', 'altitude'];
  return keysOfRide(ride)
    .filter(key => !ignoredKeys.includes(key));
}

export function keysOfRide(ride) {
  const headerFields = ride.reduce((allHeaders, rideItem) => {
    // eslint-disable-next-line no-shadow
    return Object.keys(rideItem).reduce((allHeaders, headerKey) => {
      if (!allHeaders.includes(headerKey)) {
        return allHeaders.concat([headerKey]);
      }
      return allHeaders;
    }, allHeaders);
  }, ['timestamp']);
  return headerFields;
}

export function squashChartData(data, points) {
  const window = Math.ceil(data.length / points);
  let squashed = [];

  for (let i = 0; i < data.length; i += window) {
    // calc avg time and value in window
    if ((data.length - i) < window) {
      squashed.push(avgData(data.slice(i, data.length)));
    } else {
      squashed.push(avgData(data.slice(i, i + window)));
    }
  }
  return squashed;
}

function avgData(data) {
  const summed = data.reduce((sum, item) => ({
    timestamp: sum.timestamp + item.timestamp,
    value: sum.value + item.value,
  }), {timestamp: 0, value: 0});

  return {
    timestamp: summed.timestamp / data.length,
    value: summed.value / data.length,
  };
}
