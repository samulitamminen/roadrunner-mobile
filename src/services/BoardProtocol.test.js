import {
  numberToBytes,
  bytesToNumber,
} from './BoardProtocol';

describe('BoardProtocol', () => {
  it('converts number to two bytes of binary', () => {
    const number = 16737; // 0x4161 (0x41 = 'A', 0x61 = 'a')
    expect(numberToBytes(number)).toBe('Aa');
  });

  it('handles all 16 bits when converting number to bytes', () => {
    const number = 0xFFFF;
    expect(numberToBytes(number).charCodeAt(0)).toBe(255);
    expect(numberToBytes(number).charCodeAt(1)).toBe(255);
  });

  it('converts two bytes of binary data to number', () => {
    const bytes = 'Aa';
    expect(bytesToNumber(bytes)).toBe(16737);
  });

  it('handles all 16 bits when converting bytes to number', () => {
    const bytes = numberToBytes(0xFFFF);
    expect(bytes.charCodeAt(0)).toBe(0xFF);
    expect(bytes.charCodeAt(1)).toBe(0xFF);
    expect(bytesToNumber(bytes)).toBe(0xFFFF);
  });
});
