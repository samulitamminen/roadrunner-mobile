// @flow
import type {Frame, ParameterType} from '../Types';

import {Buffer} from 'buffer';

export const Parameter = {
  LiveMode: 'L',
  Temperature: 'T',
  Voltage: 'V',
  Throttle: 'H',
  MotorSpeed: 'M',
  ActualSpeed: 'A',
  DutyCycle: 'D',
  MotorCurrent: 'C',
  WheelDiameter: 'd',
  WheelGearTooth: 'w',
  MotorGearTooth: 'm',
  MotorKVValue: 'k',
};

//eslint-disable-next-line complexity
export function parameterName(parameter: ParameterType): string {
  switch (parameter) {
    case Parameter.LiveMode: return 'Live Mode';
    case Parameter.Temperature: return 'Temperature';
    case Parameter.Voltage: return 'Voltage';
    case Parameter.Throttle: return 'Throttle';
    case Parameter.MotorSpeed: return 'Motor Speed';
    case Parameter.ActualSpeed: return 'Actual Speed';
    case Parameter.DutyCycle: return 'Duty Cycle';
    case Parameter.MotorCurrent: return 'Motor Current';
    case Parameter.WheelDiameter: return 'Wheel Diameter';
    case Parameter.WheelGearTooth: return 'Wheel Gear Tooth';
    case Parameter.MotorGearTooth: return 'Motor Gear Tooth';
    case Parameter.MotorKVValue: return 'Motor KV Value';
    default: return '';
  }
}

//eslint-disable-next-line complexity
export function parameterUnit(parameter: ParameterType): string {
  switch (parameter) {
    case Parameter.Temperature: return '° C';
    case Parameter.Voltage: return 'V';
    case Parameter.Throttle: return '%';
    case Parameter.MotorSpeed: return 'rpm';
    case Parameter.ActualSpeed: return 'km/h';
    case Parameter.DutyCycle: return '%';
    case Parameter.MotorCurrent: return 'A';
    case Parameter.WheelDiameter: return 'mm';
    default: return '';
  }
}

export function isValidParameter(parameter: ParameterType): boolean {
  return Object.keys(Parameter)
    .find(param => Parameter[param] === parameter) ? true : false;
}

export const END_OF_FRAME = '\r';

export function composeReadFrame(parameterId: ParameterType): string {
  const frame = {
    timestamp: Date.now(),
    parameterId,
    readOrWrite: 'R',
    value: 0,
  };
  return frameToBase64(frame);
}

export function composeWriteFrame(parameterId: ParameterType, value: number): string {
  const frame = {
    timestamp: Date.now(),
    parameterId,
    readOrWrite: 'W',
    value,
  };
  return frameToBase64(frame);
}

export function numberToBytes(number: number): string {
  let integer = parseInt(number);
  if (isNaN(integer)) {
    console.warn('Tried to write non-integer value to parameter!');
    integer = 0;
  }
  const buffer = Buffer.alloc(2);
  buffer.writeUInt16BE(integer);
  const bytes = String.fromCharCode(buffer.readUInt8(0)) + String.fromCharCode(buffer.readUInt8(1));
  return bytes;
}

export function bytesToNumber(bytes: string): number {
  const buffer = new Buffer(bytes, 'ascii');
  const number = buffer.readUInt16BE();
  return number;
}

export function stringToBase64(frameString: string): string {
  const buffer = Buffer.alloc(5);
  for (let i = 0; i < 5; i++) {
    buffer.writeUInt8(frameString.charCodeAt(i), i);
  }
  return buffer.toString('base64');
}

export function base64ToString(base64string: string): string {
  const buffer = new Buffer(base64string, 'base64');
  const frameString = buffer.toString('binary');
  return frameString;
}

export function frameToBase64(frame: Frame): string {
  let frameString = frame.parameterId +
    frame.readOrWrite +
    numberToBytes(frame.value) +
    END_OF_FRAME;

  return stringToBase64(frameString);
}

export function base64ToFrame(base64: string): Frame {
  const buffer = new Buffer(base64, 'base64');
  const frameString = buffer.toString('binary');

  const parameterId = frameString[0];
  const readOrWrite = frameString[1];
  const value = handleDecimalValue(parameterId, buffer.readUInt16BE(2));

  return {
    timestamp: Date.now(),
    parameterId,
    readOrWrite,
    value,
  };
}

// Some decimal values are sent like Temperature 237 => 23.7
export function handleDecimalValue(parameter: ParameterType, value: number): number {
  switch (parameter) {
    case Parameter.Temperature:
    case Parameter.Voltage:
    case Parameter.Throttle:
    case Parameter.ActualSpeed:
    case Parameter.MotorCurrent:
      return value / 10;
    default:
      return value;
  }
}
