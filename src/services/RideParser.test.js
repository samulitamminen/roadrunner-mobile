import {
  rideToCsv,
} from './RideParser';
import {ride} from './ride.mock';

describe('RideParser', () => {
  it('converts ride to csv string', () => {
    const csv = rideToCsv(ride);
    expect(csv).toMatchSnapshot();
  });
});
