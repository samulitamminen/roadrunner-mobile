// @flow
import type {
  Ride,
} from '../Types';

import {keysOfRide} from './rideHelpers';

export function rideToCsv(ride: Ride): string {
  const allHeaders = keysOfRide(ride);
  const csv = toCsv(ride, allHeaders);
  return csv;
}

export function toCsv(ride: Ride, fields: [string]): string {
  const delimeter = ',';
  const lineEnding = '\n';

  const headerLine = fields.join(',') + '\n';

  return ride.reduce((csvString, lineData) => {
    let line = '';
    for (let i = 0; i < fields.length; i++) {
      const field = fields[i];
      let fieldData = lineData[field] ;
      if (fieldData === undefined) { // eslint-disable-line no-undefined
        fieldData = '';
      }
      line += fieldData;
      if (i < fields.length - 1) {
        line += delimeter;
      }
    }
    return csvString + line + lineEnding;
  }, headerLine);
}
