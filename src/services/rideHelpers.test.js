import {
  duration,
  coordinates,
  avgParameter,
  keysOfRide,
  parametersOfRide,
  squashChartData,
} from './rideHelpers';
import {ride} from './ride.mock';

describe('rideHelpers', () => {
  it('gets duration of the ride', () => {
    expect(duration(ride)).toMatchSnapshot();
  });

  it('gets coordinates from the ride', () => {
    expect(coordinates(ride)).toMatchSnapshot();
  });

  it('gets average value of a parameter', () => {
    expect(avgParameter('T', ride)).toMatchSnapshot();
  });

  it('gets keys from the ride', () => {
    expect(keysOfRide(ride)).toMatchSnapshot();
  });

  it('gets parameters from the ride', () => {
    expect(parametersOfRide(ride)).toMatchSnapshot();
  });

  it('squashes data for the chart', () => {
    const chartData = [
      {timestamp: 1486626141100, value: 1},
      {timestamp: 1486626141200, value: 2},
      {timestamp: 1486626141300, value: 3},
      {timestamp: 1486626141000, value: 4},
      {timestamp: 1486626142000, value: 5},
      {timestamp: 1486626143000, value: 6},
      {timestamp: 1486626141010, value: 7},
      {timestamp: 1486626141020, value: 8},
    ];
    expect(squashChartData(chartData, 3)).toMatchSnapshot();
    expect(squashChartData(chartData, 1)).toMatchSnapshot();
    expect(squashChartData(chartData, 8)).toMatchSnapshot();
  });
});
