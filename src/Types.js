// @flow

export type Frame = {
  timestamp: number,
  parameterId: ParameterType,
  readOrWrite: 'W' | 'R',
  value: number,
};

export type ParameterType = string;

export type RideItem = {
  timestamp: number,
  value: number,
};

export type RideData = {[ParameterType]: [RideItem]};

export type Ride = [Object]

export type Location = Object;

export type BoardState = {
  isInLiveMode: boolean,
  receivedData: RideData,
  savedRides: [?Ride],
  locations: [?Location],
};

export type Action = {type: string};
export type Dispatch = (action: Action | ThunkAction) => any;
export type ThunkAction = (dispatch: Dispatch, getState: GetState) => any;
export type GetState = () => Object;
