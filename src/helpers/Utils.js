// @flow

// Chainable delay
// Usage: await delay(500);
export function delay(duration: number): Promise<Function> {
  return new Promise(resolve => setTimeout(resolve, duration));
}

export function round(number: number, precision: number): number {
  return Math.round(number / precision) * precision;
}

export function msToHHMMSS(ms: number): string {
  let seconds = parseInt((ms / 1000) % 60);
  let minutes = parseInt((ms / (1000 * 60)) % 60);
  let hours = parseInt((ms / (1000 * 60 * 60)) % 24);

  seconds = (seconds < 10) ? '0' + seconds : seconds;
  minutes = (minutes < 10) ? '0' + minutes : minutes;
  hours = (hours < 10) ? '0' + hours : hours;

  return `${hours}:${minutes}:${seconds}`;
}
