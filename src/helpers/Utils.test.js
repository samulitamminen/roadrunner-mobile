import {
  round,
  msToHHMMSS,
} from './Utils';

describe('round', () => {
  it('rounds with decades', () => {
    expect(round(1001, 1000)).toBe(1000);
    expect(round(999, 1000)).toBe(1000);
    expect(round(499, 1000)).toBe(0);
    expect(round(1501, 1000)).toBe(2000);
  });
  it('rounds with 200', () => {
    expect(round(1001, 200)).toBe(1000);
    expect(round(199, 200)).toBe(200);
    expect(round(499, 200)).toBe(400);
    expect(round(1501, 200)).toBe(1600);
  });
  it('rounds with 50', () => {
    expect(round(1001, 50)).toBe(1000);
    expect(round(174, 50)).toBe(150);
    expect(round(175, 50)).toBe(200);
    expect(round(449, 50)).toBe(450);
    expect(round(1501, 50)).toBe(1500);
  });
});

describe('msToHHMMSS', () => {
  it('handles zero', () => {
    expect(msToHHMMSS(0)).toBe('00:00:00');
  });
  it('handles seconds', () => {
    expect(msToHHMMSS(1000)).toBe('00:00:01');
    expect(msToHHMMSS(2400)).toBe('00:00:02');
    expect(msToHHMMSS(56200)).toBe('00:00:56');
  });
  it('handles minutes', () => {
    expect(msToHHMMSS(60000)).toBe('00:01:00');
    expect(msToHHMMSS(75100)).toBe('00:01:15');
    expect(msToHHMMSS(360000)).toBe('00:06:00');
  });
  it('handles hours', () => {
    expect(msToHHMMSS(9000000)).toBe('02:30:00');
    expect(msToHHMMSS(1000 * 60 * 60 * 4)).toBe('04:00:00');
  });
});
