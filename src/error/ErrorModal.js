import React from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import {Actions} from 'react-native-router-flux';

const ErrorModal = ({message, error}) => {
  console.log('Error: ', message, error);
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Oops!</Text>
      <Text>{message}</Text>
      <TextInput
        style={styles.error}
        editable={false}
        multiline
        value={JSON.stringify(error, null, 2) || ''}
      />
      <Text style={styles.close} onPress={() => Actions.pop()}>Close</Text>
    </View>
  );
};

ErrorModal.propTypes = {
  message: React.PropTypes.string.isRequired,
  error: React.PropTypes.any,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  title: {
    marginBottom: 20,
    marginTop: 40,
    fontSize: 24,
  },
  error: {
    flex: 1,
    alignSelf: 'stretch',
    borderColor: 'gray',
    borderWidth: 1,
    fontFamily: (Platform.OS === 'ios') ? 'Courier New' : 'monospace',
  },
  close: {
    padding: 40,
    color: 'blue',
  },
});

export default ErrorModal;
