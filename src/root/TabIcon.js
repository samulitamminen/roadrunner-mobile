import React from 'react';
import {
  View,
  Text,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

import {Color} from '../styles/Styles';

const TabIcon = ({selected, title}) => {
  const color = selected ? styles.iconSelected.color : styles.icon.color;
  const iconName = iconForTab(title);

  return (
    <View style={styles.container}>
      <Icon name={iconName} size={24} color={color} />
      <Text style={selected ? styles.iconSelected : styles.icon}>{title}</Text>
    </View>
  );
};

TabIcon.propTypes = {
  selected: React.PropTypes.bool,
  title: React.PropTypes.string.isRequired,
};

function iconForTab(title) {
  switch (title) {
    case 'Ride':
      return 'gps-fixed';
    case 'Connection':
      return 'bluetooth';
    case 'Dashboard':
      return 'dashboard';
    default:
      return 'error';
  }
}

const styles = {
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    color: Color.silver,
  },
  iconSelected: {
    color: Color.clouds,
    fontWeight: 'bold',
  },
};

export default TabIcon;
