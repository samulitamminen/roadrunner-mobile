import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Router, Scene, Actions} from 'react-native-router-flux';

import * as selectors from '../redux/ble/Reducer';
import ErrorModal from '../error/ErrorModal';
import ConnectionView from '../connection/ConnectionView';
import DashboardView from '../dashboard/DashboardView';
import RideListView from '../ride/RideListView';
import RideDetailsView from '../ride/RideDetailsView';
import RideDetailsMapView from '../ride/RideDetailsMapView';
import RideLiveView from '../ride/RideLiveView';
import TabIcon from './TabIcon';
import {Color} from '../styles/Styles';

class RootComponent extends Component {

  componentDidMount() {
    if (!this.props.isConnected) {
      Actions.connection();
    }
  }

  render() {
    return (
      <Router hideNavBar>
        <Scene key='root'>
          <Scene
            key='tabbar'
            tabs={true}
            tabBarStyle={styles.tabBar}
          >
            <Scene key='dashboard' title='Dashboard' icon={TabIcon}>
              <Scene
                key='dashboardView'
                component={DashboardView}
                title='Dashboard'
                type='replace'
              />
            </Scene>
            <Scene key='ride' title='Ride' icon={TabIcon}>
              <Scene
                key='rideView'
                component={RideListView}
                title='Ride'
                type='replace'
              />
              <Scene
                key='rideDetailsView'
                component={RideDetailsView}
                title='Ride Details'
              />
              <Scene
                key='rideDetailsMapView'
                component={RideDetailsMapView}
                title='Ride Map'
              />
              <Scene
                key='rideLiveView'
                component={RideLiveView}
                title='Current Ride'
              />
            </Scene>
            <Scene key='connection' title='Connection' icon={TabIcon}>
              <Scene
                key='connectionView'
                component={ConnectionView}
                title='Connection'
                type='replace'
              />
            </Scene>
          </Scene>
          <Scene key='error' component={ErrorModal} title='Error' direction='vertical' />
        </Scene>
      </Router>
    );
  }
}

RootComponent.propTypes = {
  isConnected: React.PropTypes.bool,
  startScan: React.PropTypes.func,
};

const styles = {
  tabBar: {
    backgroundColor: Color.wetAsphalt,
  },
};

const mapStateToProps = state => ({
  isConnected: selectors.isConnected(state),
});

export default connect(mapStateToProps, null)(RootComponent);
