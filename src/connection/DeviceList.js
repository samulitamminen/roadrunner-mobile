import React from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {
  StyleSheet,
  View,
  ListView,
} from 'react-native';

import {setSelectedDevice} from '../redux/ble/Actions';
import {getDeviceList} from '../redux/ble/Reducer';
import ListItem from '../components/ListItem';

const dataSource = new ListView.DataSource({rowHasChanged: (r1, r2) => r1.id !== r2.id});

const DeviceList = ({devices, connectToDevice}) => (
  <View style={styles.container}>
    <ListView
      dataSource={devices}
      renderRow={(rowData) =>
        <ListItem
          primaryText={rowData.name}
          secondaryText={rowData.id}
          onPress={() => connectToDevice(rowData.id)}
        />
      }
      enableEmptySections
    />
  </View>
);

const styles = StyleSheet.create({
  container: {
    alignSelf: 'stretch',
  },
});

DeviceList.propTypes = {
  devices: React.PropTypes.object,
  connectToDevice: React.PropTypes.func,
};

const mapStateToProps = state => ({
  devices: dataSource.cloneWithRows(filteredDeviceList(state)),
});

function filteredDeviceList(state) {
  const selectedDevice = state.ble.selectedDevice || {};
  return getDeviceList(state).filter((device) => device.id !== selectedDevice.id);
}

const mapDispatchToProps = dispatch => bindActionCreators({
  connectToDevice: setSelectedDevice,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(DeviceList);
