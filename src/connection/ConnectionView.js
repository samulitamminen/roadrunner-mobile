import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

import * as bleActions from '../redux/ble/Actions';
import * as selectors from '../redux/ble/Reducer';
import MyBoard from './MyBoard';
import DeviceList from './DeviceList';
import {Color} from '../styles/Styles';
import Container from '../components/Container';
import NavigationBar from '../components/NavigationBar';
import SectionHeader from '../components/SectionHeader';

const scanningDuration = 3000;

class ConnectionView extends Component {
  componentDidMount() {
    setTimeout(() => this.refreshScannedDevices(), 100);
  }

  refreshScannedDevices() {
    if (!this.props.isScanning) {
      this.props.startScan();
    }
    setTimeout(() => {
      this.props.stopScan();
    }, scanningDuration);
  }

  // eslint-disable-next-line react/no-multi-comp
  rightNavBarItem() {
    return this.props.isScanning
      ? <ActivityIndicator color={Color.wetAsphalt} animating={this.props.isScanning} />
      : <TouchableOpacity onPress={() => this.refreshScannedDevices()}>
          <Icon name='refresh' size={24} color={Color.wetAsphalt} />
        </TouchableOpacity>;

  }

  render() {
    return (
      <Container>
        <NavigationBar
          title='Connection'
          right={this.rightNavBarItem()}
        />

        <SectionHeader title='My Board' />
        <MyBoard />

        <SectionHeader title='Found Devices' />
        <DeviceList />
      </Container>
    );
  }
}

ConnectionView.propTypes = {
  isScanning: React.PropTypes.bool,
  startScan: React.PropTypes.func,
  stopScan: React.PropTypes.func,
};

const mapStateToProps = state => ({
  isScanning: selectors.isScanning(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  startScan: bleActions.startScan,
  stopScan: bleActions.stopScan,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ConnectionView);
