import React from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {
  StyleSheet,
  View,
  Text,
  TouchableHighlight,
} from 'react-native';

import * as actions from '../redux/ble/Actions';
import * as selectors from '../redux/ble/Reducer';
import {Color, Default} from '../styles/Styles';

const MyBoard = (props) => {
  // eslint-disable-next-line react/no-multi-comp
  function connectionStatusLabel() {
    if (isInRange()) {
      return props.isConnected ? disconnectButton() : connectButton();
    } else {
      return <Text style={styles.status}>NOT IN RANGE</Text>;
    }
  }

  // eslint-disable-next-line react/no-multi-comp
  function connectButton() {
    return (
      <TouchableHighlight onPress={() => props.connectToDevice(props.selectedDevice.id)}>
        <Text style={styles.statusConnected}>TAP TO CONNECT</Text>
      </TouchableHighlight>
    );
  }

  // eslint-disable-next-line react/no-multi-comp
  function disconnectButton() {
    return (
      <TouchableHighlight onPress={props.disconnectFromDevice}>
        <Text style={styles.statusDisconnected}>TAP TO DISCONNECT</Text>
      </TouchableHighlight>
    );
  }

  function isInRange() {
    return props.scannedDevices.find((device) => device.id === props.selectedDevice.id);
  }

  return (
    <View style={styles.container}>
      <View style={styles.topRow}>
        <Text style={styles.name}>
          {props.selectedDevice.name || 'Board not selected'}
        </Text>
        {connectionStatusLabel()}
      </View>
      <Text style={styles.id}>
        {props.selectedDevice.id || 'Latest board connected will show here.'}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: Default.padding,
    borderBottomWidth: 1,
    borderBottomColor: Color.silver,
    backgroundColor: Color.white,
  },
  topRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  name: {
    fontSize: 20,
  },
  status: {
    color: Color.silver,
  },
  statusConnected: {
    color: Color.nephritis,
  },
  statusDisconnected: {
    color: Color.carrot,
  },
  id: {
    fontSize: 14,
    paddingTop: 8,
    color: Color.silver,
  },
});

MyBoard.propTypes = {
  isConnected: React.PropTypes.bool,
  selectedDevice: React.PropTypes.object,
  scannedDevices: React.PropTypes.array,

  connectToDevice: React.PropTypes.func,
  disconnectFromDevice: React.PropTypes.func,
};

const mapStateToProps = state => ({
  isConnected: selectors.isConnected(state),
  selectedDevice: selectors.getSelectedDevice(state) || {},
  scannedDevices: selectors.getDeviceList(state) || [],
});

const mapDispatchToProps = dispatch => bindActionCreators({
  connectToDevice: actions.setSelectedDevice,
  disconnectFromDevice: actions.disconnectFromDevice,
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(MyBoard);
