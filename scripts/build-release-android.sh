#!/bin/sh

# $ROADRUNNER_KEYSTORE_PASSWORD
source ./config

# cd to android directory as gradlew works only there
cd ../android

echo "Assemble release"
./gradlew assembleRelease

echo "Sign jar"
jarsigner -verbose -keystore ./app/roadrunner-release.keystore ./app/build/outputs/apk/app-release-unsigned.apk roadrunner-release -storepass $ROADRUNNER_KEYSTORE_PASSWORD

echo "Zipalign"
zipalign -f -v 4 ./app/build/outputs/apk/app-release-unsigned.apk ./app/build/outputs/apk/app-release.apk

echo "Install to device"
adb install -r ./app/build/outputs/apk/app-release.apk

echo "All done."
